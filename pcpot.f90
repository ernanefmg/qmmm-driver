module pcpot_m
   !! This module contains subroutines to calculate the potential
   !! due to solvent molecules at each point of the mesh.
   implicit none

   public :: pcpot
   public :: pcpot_ewald

contains

  subroutine pcpot( na_qm, na_mm, natot, ntpl, ntm, r, qmcell, pc, &
                    rcut_rho, Rho, Vqm, lattice_type )
    !! Calculates the potential using a cut-off scheme.
    use precision     , only : dp, grid_p
    use qmmm_neighbour, only : num_mmvecs, grid_veclist, grid_nr
    use qmmm_pbc      , only : reccel, pbc_displ_vector

    implicit none
    integer     , intent(in)  :: na_qm
      !! Number of QM atoms.
    integer     , intent(in)  :: na_mm
      !! Number of MM atoms.
    integer     , intent(in)  :: natot
      !! Total (QM+MM) number of atoms.
    integer     , intent(in)  :: ntpl
      !! Total number of grid points.
    integer     , intent(in)  :: ntm(3)
      !! Number of mesh points along each axis.
    real(dp)    , intent(in)  :: r(3,natot)
      !! Atomic positions.
    real(dp)    , intent(in)  :: pc(na_mm)
      !! Atomic (classical) partial charges.
    real(dp)    , intent(in)  :: qmcell(3,3)
      !! Unit cell vectors.
    real(dp)    , intent(in)  :: rcut_rho
      !! Cut-off radius grid point-charge interactions.
    real(grid_p), intent(in)  :: Rho(ntpl)
      !! Electron density in the grid.
    character   , intent(in)  :: lattice_type
      !! Type of cell lattice.
    real(grid_p), intent(out) :: Vqm(ntpl)
      !! Electrostatic potential for the QM region.

    integer  :: ix, iy, iz, iat, js, icrd, ivec, imesh
    real(dp) :: ym, xm, zm, dist, rcut_rho2, rcut_rho_A, drij(3), kcell(3,3)

    Vqm(:) = 0.0_grid_p
    if ( num_mmvecs == 0 ) return
    rcut_rho_A = rcut_rho / 0.529177_dp
    rcut_rho2  = rcut_rho_A * rcut_rho_A
    rcut_rho_A = 1.0_dp / rcut_rho_A

    ! Gets reciprocal lattice vectors.
    call reccel( 3, qmcell, kcell, 0 )

    ! Loop over the mesh points
    do iz = 0, ntm(3) -1
    do iy = 0, ntm(2) -1
    do ix = 0, ntm(1) -1
      imesh = 1 + ix + ntm(1) * iy + ntm(1) * ntm(2) * iz

      ! Only for grid points where there is density of charge
      if ( abs( Rho(imesh) ) > 0.0_dp ) then
        xm = qmcell(1,1) * ix / ntm(1) + qmcell(1,2) * iy / ntm(2) &
           + qmcell(1,3) * iz / ntm(3)
        ym = qmcell(2,1) * ix / ntm(1) + qmcell(2,2) * iy / ntm(2) &
           + qmcell(2,3) * iz / ntm(3)
        zm = qmcell(3,1) * ix / ntm(1) + qmcell(3,2) * iy / ntm(2) &
           + qmcell(3,3) * iz / ntm(3)

        do iat = 1, num_mmvecs ! Loop over MM atoms
          js = grid_veclist(iat)

          if ( lattice_type == 'D' ) then
            drij(1) = xm - r(1,js) + grid_nr(1,iat) * qmcell(1,1)
            drij(2) = ym - r(2,js) + grid_nr(2,iat) * qmcell(2,2)
            drij(3) = zm - r(3,js) + grid_nr(3,iat) * qmcell(3,3)
          else
            drij(1) = xm - r(1,js)
            drij(2) = ym - r(2,js)
            drij(3) = zm - r(3,js)

            do icrd = 1, 3
            do ivec = 1, 3
              drij(icrd) = drij(icrd) + grid_nr(ivec,iat) * qmcell(icrd,ivec)
            enddo
            enddo
          endif
          call pbc_displ_vector( lattice_type, qmcell, kcell, &
                                 drij(1), drij(2), drij(3) )
          dist = drij(1) * drij(1) + drij(2) * drij(2) + drij(3) * drij(3)

          if ( dist > rcut_rho2 ) then
            ! If our density point and point charge are too close,
            ! this might diverge. Thus, we avoid it.
            dist = 1.0_dp / sqrt( dist )
            Vqm(imesh) = Vqm(imesh) + pc(js-na_qm) * dist
          else
            Vqm(imesh) = Vqm(imesh) + pc(js-na_qm) * rcut_rho_A
          endif
        enddo ! MM atoms
      endif   ! abs(Rho(imesh)) > 0
    enddo     ! grid X
    enddo     ! grid Y
    enddo     ! grid Z

    ! Change units to Ry
    Vqm(:) = -2.0_grid_p * Vqm(:)
  end subroutine pcpot

  subroutine pcpot_ewald( na_qm, na_mm, natot, ntpl, ntm, r, qmcell, vol, pc, &
                          rcut_rho, ewald_alpha, kewald_cutoff, Rho, Vqm,     &
                          lattice_type, smoothpotential, electrode_length,    &
                          smooth_length )
    !! Calculates the potential using the Ewald summation method.
    use alloc         , only : re_alloc, de_alloc
    use functions     , only : vec_norm
    use precision     , only : dp, grid_p
    use qmmm_neighbour, only : num_mmvecs, grid_veclist, grid_nr
    use qmmm_pbc      , only : reccel, pbc_displ_vector
    use sys           , only : die

    implicit none
    integer     , intent(in)  :: na_qm
      !! Number of QM atoms.
    integer     , intent(in)  :: na_mm
      !! Number of MM atoms.
    integer     , intent(in)  :: natot
      !! Total (QM+MM) number of atoms.
    integer     , intent(in)  :: ntpl
      !! Total number of grid points.
    integer     , intent(in)  :: ntm(3)
      !! Number of mesh points along each axis.
    real(dp)    , intent(in)  :: r(3,natot)
      !! Atomic positions.
    real(dp)    , intent(in)  :: pc(na_mm)
      !! Atomic (classical) partial charges.
    real(dp)    , intent(in)  :: qmcell(3,3)
      !! Unit cell vectors.
    real(dp)    , intent(in)  :: vol
      !! Unit cell volume.
    real(dp)    , intent(in)  :: rcut_rho
      !! Cut-off radius grid point-charge interactions.
    real(dp)    , intent(in)  :: ewald_alpha
      !! Ewald alpha factor.
    real(dp)    , intent(in)  :: kewald_cutoff
      !! Ewald coefficient in the reciprocal space.
    real(grid_p), intent(in)  :: Rho(ntpl)
      !! Electron density in the grid.
    character   , intent(in)  :: lattice_type
      !! Type of cell lattice.
    real(dp)    , intent(in)  :: electrode_length
      !! Length of the electrode.
    real(dp)    , intent(in)  :: smooth_length
      !! Length of the smoothing region.
    logical     , intent(in)  :: smoothpotential
      !! Whether we smooth the potential or not.
    real(grid_p), intent(out) :: Vqm(ntpl)
      !! Electrostatic potential for the QM region.

    integer  :: ix, iy, iz, iat, js, icrd, ivec, imesh, n1, n2, n3, &
                n1max, n2max, n3max, Len_to_Mesh_ele, Len_to_Mesh_smth
    real(dp) :: ym, xm, zm, dist, rcut_rho2, rcut_rho_A, drij(3),   &
                qm_ewald_alpha, qm_sqrt_ewald_alpha, const2, kmod2, &
                const_sin, kr, twopi, krecip(3), qm_kewald_cutoff,  &
                kcut2, kcell(3,3), Mesh_to_Length, smooth_plus_electrode

    integer , parameter :: ewald_nmax = 20
    real(dp), pointer   :: S_real(:,:,:), S_imag(:,:,:)

    Vqm(:) = 0.0_grid_p
    if ( num_mmvecs == 0 ) return
    nullify( S_real, S_imag )
    call re_alloc( S_real, -ewald_nmax, ewald_nmax, -ewald_nmax, ewald_nmax,&
                  -ewald_nmax, ewald_nmax, 'S_real', 'pcpot_ewald' )
    call re_alloc( S_imag, -ewald_nmax, ewald_nmax, -ewald_nmax, ewald_nmax,&
                  -ewald_nmax, ewald_nmax, 'S_imag', 'pcpot_ewald' )

    rcut_rho_A = rcut_rho / 0.529177_dp
    rcut_rho2  = rcut_rho_A * rcut_rho_A

    qm_kewald_cutoff    = kewald_cutoff * 0.529177_dp
    qm_ewald_alpha      = ewald_alpha   * 0.529177_dp * 0.529177_dp
    qm_sqrt_ewald_alpha = sqrt( qm_ewald_alpha )

    twopi  = 2.0_dp * acos( -1.0_dp )
    const2 = 2.0_dp * twopi / vol

    ! We calculate the reciprocal lattice vectors
    call reccel( 3, qmcell, kcell, 0 )

    drij(:) = kcell(1,:)
    n1max = INT( qm_kewald_cutoff / ( twopi * vec_norm( drij, 3) ) )
    if ( n1max > ewald_nmax ) call die( 'mmforce_ewald: n1max > ewald_nmax' )

    drij(:) = kcell(2,:)
    n2max = INT( qm_kewald_cutoff / ( twopi * vec_norm( drij, 3) ) )
    if ( n2max > ewald_nmax ) call die( 'mmforce_ewald: n2max > ewald_nmax' )

    drij(:) = kcell(3,:)
    n3max = INT( qm_kewald_cutoff / ( twopi * vec_norm( drij, 3) ) )
    if ( n3max > ewald_nmax ) call die( 'mmforce_ewald: n3max > ewald_nmax' )

    kcut2 = qm_kewald_cutoff * qm_kewald_cutoff


    ! This is done to avoid OpenMP Warnings.
    dist = 0_dp; kmod2 = 0_dp; kr = 0_dp
    xm   = 0_dp; ym    = 0_dp; zm = 0_dp
    Mesh_to_Length = 0_dp; imesh = 0; js = 0

    ! Real part of the Ewald summation
    ! Loop over the mesh points
!$OMP PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(Vqm,S_real,S_imag,Rho)
!$OMP BARRIER
!$OMP CRITICAL
!$OMP END CRITICAL

!$OMP DO SCHEDULE(DYNAMIC,2)
    do iz = 0, ntm(3) -1
    do iy = 0, ntm(2) -1
    do ix = 0, ntm(1) -1
      imesh = 1 + ix + ntm(1) * iy + ntm(1) * ntm(2) * iz

      ! We use only grid points where there is density of charge.
      if ( abs(Rho(imesh)) > 0.0_dp ) then
        xm = qmcell(1,1) * ix / ntm(1) + qmcell(1,2) * iy / ntm(2) &
           + qmcell(1,3) * iz / ntm(3)
        ym = qmcell(2,1) * ix / ntm(1) + qmcell(2,2) * iy / ntm(2) &
           + qmcell(2,3) * iz / ntm(3)
        zm = qmcell(3,1) * ix / ntm(1) + qmcell(3,2) * iy / ntm(2) &
           + qmcell(3,3) * iz / ntm(3)

        ! Loop over MM atoms
        do iat = 1, num_mmvecs
          js = grid_veclist(iat)

          if ( lattice_type == 'D' ) then
            drij(1) = xm - r(1,js) + grid_nr(1,iat) * qmcell(1,1)
            drij(2) = ym - r(2,js) + grid_nr(2,iat) * qmcell(2,2)
            drij(3) = zm - r(3,js) + grid_nr(3,iat) * qmcell(3,3)
          else
            drij(1) = xm - r(1,js)
            drij(2) = ym - r(2,js)
            drij(3) = zm - r(3,js)

            do iCrd = 1, 3
            do iVec = 1, 3
              drij(iCrd) = drij(iCrd) + grid_nr(iVec,iat) * qmcell(iCrd,iVec)
            enddo
            enddo
          endif
          call pbc_displ_vector( lattice_type, qmcell, kcell, &
                                 drij(1), drij(2), drij(3) )
          dist = drij(1) * drij(1) + drij(2) * drij(2) + drij(3) * drij(3)

          ! Calculation of the external potential due to point charges plus
          ! Gaussian distributions.

          ! Real-space sum
          if ( dist > rcut_rho2 ) then
            dist       = sqrt( dist )
            Vqm(imesh) = Vqm(imesh) + pc(js-na_qm) * &
                         erfc( qm_sqrt_ewald_alpha * dist ) / dist
          else
            Vqm(imesh) = Vqm(imesh) + pc(js-na_qm) * &
                         erfc( qm_sqrt_ewald_alpha * rcut_rho_A ) / rcut_rho_A
          endif
        enddo ! at sv
      endif   ! abs(Rho(imesh)) > 0
    enddo     ! grid X
    enddo     ! grid Y
    enddo     ! grid Z
!$OMP END DO
!$OMP END PARALLEL

    ! Reciprocal part of the Ewald summation.
    ! Calculate structure factors for atoms.
    S_real(:,:,:) = 0.0_dp
    S_imag(:,:,:) = 0.0_dp

    if ( lattice_type == 'D' ) then
!$OMP PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(Vqm,S_real,S_imag,Rho)
!$OMP DO SCHEDULE(DYNAMIC,2)
      do n1 = -n1max, n1max
      do n2 = -n2max, n2max
      do n3 = -n3max, n3max
        if ( (n1 == 0) .and. (n2 == 0) .and. (n3 == 0) ) cycle

        ! Loop over MM atoms
        krecip(1) = n1 * twopi * kcell(1,1)
        krecip(2) = n2 * twopi * kcell(2,2)
        krecip(3) = n3 * twopi * kcell(3,3)
        kmod2 = krecip(1) * krecip(1) + krecip(2) * krecip(2) &
              + krecip(3) * krecip(3)

        if ( kmod2 > kcut2 ) cycle

        ! Loop over MM atoms
        do iat = na_qm+1, natot
          kr = krecip(1) * r(1,iat) + krecip(2) * r(2,iat) &
             + krecip(3) * r(3,iat)
          S_real(n1,n2,n3) = S_real(n1,n2,n3) + pc(iat-na_qm) * cos(kr)
          S_imag(n1,n2,n3) = S_imag(n1,n2,n3) + pc(iat-na_qm) * sin(kr)
        enddo
      enddo
      enddo
      enddo
!$OMP END DO
!$OMP END PARALLEL
    else ! Lattice
!$OMP PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(Vqm,S_real,S_imag)
!$OMP DO SCHEDULE(DYNAMIC,2)
      do n1 = -n1max, n1max
      do n2 = -n2max, n2max
      do n3 = -n3max, n3max
        if ( (n1 == 0) .and. (n2 == 0) .and. (n3 == 0) ) cycle

        krecip(1) = twopi * ( n1 * kcell(1,1) + n2 * kcell(2,1) &
                            + n3 * kcell(3,1) )
        krecip(2) = twopi * ( n1 * kcell(1,2) + n2 * kcell(2,2) &
                            + n3 * kcell(3,2) )
        krecip(3) = twopi * ( n1 * kcell(1,3) + n2 * kcell(2,3) &
                            + n3 * kcell(3,3) )
        kmod2 = krecip(1) * krecip(1) + krecip(2) * krecip(2) &
              + krecip(3) * krecip(3)

        if ( kmod2 > kcut2 ) cycle

        ! Loop over MM atoms
        do iat = na_qm+1, natot
          kr = krecip(1) * r(1,iat) + krecip(2) * r(2,iat) &
             + krecip(3) * r(3,iat)
          S_real(n1,n2,n3) = S_real(n1,n2,n3) + pc(iat-na_qm) * cos(kr)
          S_imag(n1,n2,n3) = S_imag(n1,n2,n3) + pc(iat-na_qm) * sin(kr)
        enddo
      enddo
      enddo
      enddo
!$OMP END DO
!$OMP END PARALLEL
    endif ! Lattice

    ! Calculate the reciprocal part of the potential on QM grid points.
    ! Loop over the mesh points.
!$OMP PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(Vqm,S_real,S_imag,Rho) &
!$OMP& PRIVATE(imesh)
!$OMP DO SCHEDULE(DYNAMIC,2)
    do iz = 0, ntm(3) -1
    do iy = 0, ntm(2) -1
    do ix = 0, ntm(1) -1
      imesh = 1 + ix + ntm(1) * iy + ntm(1) * ntm(2) * iz

      ! We use only grid points where there is density of charge.
      if ( abs(Rho(imesh)) > 0.0_dp ) then
        xm = qmcell(1,1) * ix / ntm(1) + qmcell(1,2) * iy / ntm(2) &
           + qmcell(1,3) * iz / ntm(3)
        ym = qmcell(2,1) * ix / ntm(1) + qmcell(2,2) * iy / ntm(2) &
           + qmcell(2,3) * iz / ntm(3)
        zm = qmcell(3,1) * ix / ntm(1) + qmcell(3,2) * iy / ntm(2) &
           + qmcell(3,3) * iz / ntm(3)


        if ( lattice_type == 'D' ) then
          do n1 = -n1max, n1max
          do n2 = -n2max, n2max
          do n3 = -n3max, n3max
            if ( (n1 == 0) .and. (n2 == 0) .and. (n3 == 0) ) cycle

            krecip(1) = n1 * twopi * kcell(1,1)
            krecip(2) = n2 * twopi * kcell(2,2)
            krecip(3) = n3 * twopi * kcell(3,3)
            kmod2 = krecip(1) * krecip(1) + krecip(2) * krecip(2) &
                  + krecip(3) * krecip(3)

            if ( kmod2 > kcut2 ) cycle
            kr = krecip(1) * xm + krecip(2) * ym + krecip(3) * zm
            Vqm(imesh) = Vqm(imesh) + const2 / kmod2                 &
                       * exp( - kmod2 / (4.0_dp * qm_ewald_alpha ) ) &
                       * ( cos(kr) * S_real(n1,n2,n3)                &
                         + sin(kr) * S_imag(n1,n2,n3) )
          enddo
          enddo
          enddo
        else ! Lattice
          do n1 = -n1max, n1max
          do n2 = -n2max, n2max
          do n3 = -n3max, n3max
            if ( (n1 == 0) .and. (n2 == 0) .and. (n3 == 0) ) cycle

            krecip(1) = twopi * ( n1 * kcell(1,1) + n2 * kcell(2,1) &
                                + n3 * kcell(3,1) )
            krecip(2) = twopi * ( n1 * kcell(1,2) + n2 * kcell(2,2) &
                                + n3 * kcell(3,2) )
            krecip(3) = twopi * ( n1 * kcell(1,3) + n2 * kcell(2,3) &
                                + n3 * kcell(3,3) )
            kmod2 = krecip(1) * krecip(1) + krecip(2) * krecip(2) &
                  + krecip(3) * krecip(3)

            if ( kmod2 > kcut2 ) cycle
            kr = krecip(1) * xm + krecip(2) * ym + krecip(3) * zm
            Vqm(imesh) = Vqm(imesh) + const2 / kmod2                 &
                       * exp( - kmod2 / (4.0_dp * qm_ewald_alpha ) ) &
                       * ( cos(kr) * S_real(n1,n2,n3)                &
                         + sin(kr) * S_imag(n1,n2,n3) )
          enddo
          enddo
          enddo
        endif ! Lattice
      endif   ! abs(Rho(imesh)) > 0
    enddo     ! grid X
    enddo     ! grid Y
    enddo     ! grid Z
!$OMP END DO
!$OMP END PARALLEL

    !  Potential smoothing around the electrodes, if present.
    ! The smoothing makes some assumptions about the system:
    !
    !  * iz is the number of mesh points in z direction, and in this case, 360.
    !  * The electrode is in the region between points 1-60 and 300-360.
    !  * The smoothing is done by multiplying the final mesh by a quarter of
    !    a trigonometric function (going from 0 to 1).
    !  * The potential is set strictly to zero in the electrode region.
    !  * The size of the smoothing region is half of the electrode.
    if ( smoothpotential ) then
      write(6,*) "siesta-qmmm: WARNING: To use the smoothing function "//&
                 "the electrodes must be in the extremes of the box."
      write(6,*)

      Len_to_Mesh_ele  = electrode_length * ntm(3) / qmcell(3,3)
      Len_to_Mesh_smth = smooth_length    * ntm(3) / qmcell(3,3)
      smooth_plus_electrode = electrode_length + smooth_length

      const_sin = twopi / ( 4.0_dp * Len_to_Mesh_smth )
!$OMP PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(Vqm,S_real,S_imag,Rho)
!$OMP DO SCHEDULE(DYNAMIC,2)
      do iz = 0, ntm(3) -1
      do iy = 0, ntm(2) -1
      do ix = 0, ntm(1) -1
        imesh = 1 + ix + ntm(1) * iy + ntm(1) * ntm(2) * iz

        Mesh_to_Length = iz * qmcell(3,3) / ntm(3)

        if ( ( Mesh_to_Length < smooth_plus_electrode ) .and. &
             ( Mesh_to_Length > electrode_length      ) ) &
          Vqm(imesh) = Vqm(imesh) * ( sin( const_sin * &
                       (iz - Len_to_Mesh_ele) ) ) ** 2

        if ( ( Mesh_to_Length < (qmcell(3,3) - electrode_length     ) ) .and. &
             ( Mesh_to_Length > (qmcell(3,3) - smooth_plus_electrode) ) )     &
          Vqm(imesh) = Vqm(imesh) * ( sin( const_sin * &
                       (iz - ntm(3) -1 + Len_to_Mesh_ele) ) ) ** 2

        if ( ( Mesh_to_Length < electrode_length ) .or. &
             ( Mesh_to_Length > (qmcell(3,3) - electrode_length) ) ) &
          Vqm(imesh) = 0.0_grid_p
      enddo
      enddo
      enddo
!$OMP END DO
!$OMP END PARALLEL
      write(6,*) "siesta-qmmm: The Vext was smoothed out at the region"//&
                 " of the electrodes."
      write(6,*)
    endif ! smoothpotential

    ! Fix units
    Vqm(:) = -2.0_grid_p * Vqm(:)

    call de_alloc( S_real, 'S_real', 'pcpot_ewald' )
    call de_alloc( S_imag, 'S_imag', 'pcpot_ewald' )
  end subroutine pcpot_ewald
end module pcpot_m
