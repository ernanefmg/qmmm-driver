## Installation notes for the QMMM driver and its dependencies.

### Configuration

The QMMM driver uses a mumber of Siesta routines and modules to:

   - Implement needed functionality (e.g., molecular dynamics routines)
   - Maintain data structures (such as grid information)

In order to provide those routines, the qmmm-driver package uses a
git submodule associated to the 'siesta-library' directory.

After cloning the qmmm-driver package, do the following:

```
    cd qmmm-driver
    git submodule update --init --recursive
```

This will populate the 'siesta-library' directory with a particular
Siesta version that is appropriate for this version of the driver.
DO NOT change this Siesta version unless you know very well what
you are doing.

You can see more information about the submodule by looking in the
.gitmodules file, whose contents should be:

```
[submodule "siesta-library"]
	path = siesta-library
	url = https://gitlab.com/garalb/siesta.git
	branch = 3.1-qmmm
```

Also, you can execute the command

```
    git submodule status
```

to see the exact commit in the above branch.

### When remote download is not possible

In some computer centers outgoing network connections are not allowed, preventing
submodules (or even git working trees) from being properly configured. In this case,
the script `stage_submodules.sh` can be used on a local computer (with git installed and
full network access) to generate the full sources for the project (including submodules).
These sources can later be tarred or rsync'd to the remote computer.

Please read the top section of the script for more information and usage instructions.

## Compilation of the qmmm-driver

Take a look at the 'sample.arch.make' file in the top directory and
copy it, with the appropriate changes to suit your platform and compiler,
to siesta-library/Obj/arch.make

Then go to siesta-library/Obj and type

```
   sh ../Src/obj_setup.sh
   make
```

This will compile the necessary Siesta object files (and incidentally Siesta
itself, but see below).

Now, go back to the top directory of the qmmm-driver distribution, and take
a look at the 'siesta-qmmm.make' file. You should not need to change anything
in this file.

Now type, still in the top level:

```
     make
```

and you will get an executable file 'siesta-qmmm'.

## Using Siesta as a backend for the qmmm-driver

The Siesta executable produced in siesta-library/Obj/siesta in the previous step
can be used as the 'force and charge density calculator' needed by the qmmm-driver.
For this, you could just make a symbolic link to it in the directory where you are running
a QMMM calculation.

However, it is required for now to compile the driver (and the Siesta
library routines) in serial form. The driver is not parallelized with
MPI, and even though it *could* work if compiled in parallel,
difficult to debug issues might crop up.  A parallel version of the
*backend* siesta executable, which will give better performance, can
be compiled in an alternate directory (see siesta-library/Òbj/README).

Besides, this version of Siesta is quite old. Newer versions can be used, as long
as they have been equipped 
with some extra features to communicate properly with
the qmmm-driver:

  * reading an external potential from a file produced by the driver
  * producing a charge density file at the right moment
  * passing an optional flag through the 'siesta-as-server' interface

As of this writing, the branch

       https://gitlab.com/garalb/siesta/-/tree/4.1-qmmm

is so equipped, and tracks the 4.1 release branch.

   

