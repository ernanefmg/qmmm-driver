
module m_fill_auxrho
  !! This module contains a single subroutine that fill an auxiliary
  !! electron density on the grid.
  implicit none
  public :: fill_auxrho

contains

  subroutine fill_auxrho( na_siesta, qmcell, rclas, ntm, ntpl, atm_rcut, &
                          auxrho )

    !! Fills an auxiliary electronic density on the grid, essentially filling
    !! all of a given atom's "volume".
    use precision, only : dp, grid_p
    use sys      , only : die
    implicit none

    integer     , intent(in)    :: na_siesta
      !! Total number of atoms.
    real(dp)    , intent(in)    :: qmcell(3,3)
      !! Unit cell size.
    real(dp)    , intent(in)    :: rclas(3,na_siesta)
      !! Atomic cartesian positions.
    integer     , intent(in)    :: ntm(3)
      !! Total number of points along each axis.
    integer     , intent(in)    :: ntpl
      !! Total number of grid points.
    real(dp)    , intent(in)    :: atm_rcut(na_siesta)
      !! Maximum rcut for basis functions in each atom.
    real(grid_p), intent(inout) :: auxrho(ntpl)
      !! Electronic density on grid.

    integer  :: ia, ivec, imesh, ix, iy, iz, pix, piy, piz, nx, ny, nz, &
                ipiv(3), icr(3), info
    real(dp) :: rix, riy, riz, Bvec(3), A(3,3)

    do ia = 1, na_siesta
      Bvec(1:3) = rclas(1:3,ia)
      do ivec = 1,3
        A(1:3,ivec) = qmcell(1:3,ivec) / ntm(ivec)
      enddo

      call dgesv( 3, 1, A, 3, ipiv, Bvec, 3, info )

      if ( info /= 0 ) &
        call die( 'fill_auxrho: Failed to solve first system of equations.' )

      icr(1) = int( Bvec(1) )
      icr(2) = int( Bvec(2) )
      icr(3) = int( Bvec(3) )

      Bvec(1:3) = atm_rcut(ia)
      do ivec = 1,3
        A(1:3,ivec) = qmcell(1:3,ivec) / ntm(ivec)
      enddo

      call dgesv( 3, 1, A, 3, ipiv, Bvec, 3, info )
      if ( info /= 0 ) &
        call die( 'fill_auxrho: Failed to solve second system of equations.' )

      nx = int( Bvec(1) ) +1
      ny = int( Bvec(2) ) +1
      nz = int( Bvec(3) ) +1

      do iz = -nz, nz
      do iy = -ny, ny
      do ix = -nx, nx
        rix = real( ix, kind = dp ) / real( nx, kind = dp )
        riy = real( iy, kind = dp ) / real( ny, kind = dp )
        riz = real( iz, kind = dp ) / real( nz, kind = dp )

        if ( (rix*rix + riy*riy + riz*riz) > 1.0_dp ) cycle
        pix   = icr(1) + ix
        piy   = icr(2) + iy
        piz   = icr(3) + iz
        pix   = modulo( pix, ntm(1) )
        piy   = modulo( piy, ntm(2) )
        piz   = modulo( piz, ntm(3) )

        imesh = 1 + pix + ntm(1) * piy + ntm(1) * ntm(2) * piz
        auxrho(imesh) = 1.0_grid_p
      enddo
      enddo
      enddo
    enddo
  end subroutine fill_auxrho

end module m_fill_auxrho
