module qmmm_list_block
  !! This module contains the declaration of both blocked (i.e non-moving or
  !! constrained ) MM atoms and those atoms that are close enough to participate
  !! in QM-MM interactions (only used afterwards when writing the PDB).
  !!
  !! Basically we first attempt to read a ".lst" file with closest MM atoms. If
  !! that fails, we consider all atoms within rcut_qmm as being "QM-MM active".
  !! Then, we do the same, trying to read a ".blk" file; MM atoms specified in
  !! the file will be considered "blocked" (immovable). If no such file exists,
  !! we consider any atom BEYOND blockmm_radius to be frozen.
  !!
  !! If rcut_qmmm is greater than 99A, all MM atoms are considered to be
  !! "QM-MM active". In the same manner, if blockmm_radius is greater than
  !! 99A, no MM atoms are considered to be blocked.
  implicit none
  public :: qmmm_lst_blk

  private

contains
  subroutine qmmm_lst_blk( na_qm, na_mm, nroaa, atxres, rclas, rcut_qmmm, &
                           blockmm_radius, blockqmmm, listqmmm, slabel )

    !! Calculates the rcut and block QM-MM only in the first step. Attempts
    !! to first read an .lst or .blk file in order to avoid calculations.
    use alloc    , only : re_alloc, de_alloc
    use functions, only : dist2_v2
    use precision, only : dp

    implicit none
    integer         , intent(in)    :: na_qm
      !! Number of QM atoms.
    integer         , intent(in)    :: na_mm
      !! Number of MM atoms.
    integer         , intent(in)    :: nroaa
      !! Number of MM residues.
    integer         , intent(in)    :: atxres(nroaa)
      !! Number of atoms per residue.
    real(dp)        , intent(in)    :: rclas(3,na_qm+na_mm)
      !! Atomic positions.
    real(dp)        , intent(in)    :: rcut_qmmm
      !! Cut-off radius for QM-MM interactions.
    real(dp)        , intent(in)    :: blockmm_radius
      !! Cut-off radius for blocking (constraining) MM atoms.
    character(len=*), intent(in)    :: slabel
      !! System label (name).
    integer         , intent(inout) :: blockqmmm(na_mm)
      !! List of blocked (constrained) MM atoms.
    integer         , intent(inout) :: listqmmm(na_mm)
      !! QM-MM neighbour list (i.e. included in QM-MM interactions).

    ! Internal variables.
    character(len=255) :: fname
    integer            :: ios, iat, jat, qat, ires, iu, count
    logical            :: bloqmmm, liqmmm, found
    real(dp)           :: dist, dist2, Ang
    real(dp), pointer  :: cm(:,:)

    ! External subroutines and functions.
    external           :: io_assign, io_close

    Ang = 1.0_dp / 0.529177_dp
    ios = 0

    ! Calculate center of masses of all residues.
    nullify( cm )
    call re_alloc( cm, 1, 3, 1, nroaa, 'cm', 'qmmm_lst_blk' )

    iat = na_qm +1
    do ires = 1, nroaa
      do jat = 1, atxres(ires)
        cm(1:3,ires) = cm(1:3,ires) + rclas(1:3,iat)
        iat = iat +1
      enddo

      cm(1:3,ires) = cm(1:3,ires) / atxres(ires)
    enddo

    ! rcut QM-MM
    found  = .false.
    liqmmm = .false.

    ! Find file name and verify existence.
    fname  = trim(slabel) // '.lst'
    inquire( file = fname, exist = found )

    if ( found ) then
      call io_assign( iu )
      open( iu, file = fname, status = 'old' )

      ! Read QMMM list.
      do iat = 1, na_mm
        read( iu, *, iostat = ios) listqmmm(iat)
        call check_ios( ios, '.lst' )
      enddo

      call io_close( iu )
      write( 6, '(/a)' ) 'qm-mm: Reading QM-MM neighbours list from file.'
    else
      if ( .not. (rcut_qmmm > 99.0_dp) ) liqmmm = .true.
      if ( liqmmm ) then
        ! QM-MM neigh list
        write( 6, '(/a,f12.6)' ) 'qm-mm: cut off radius QM-MM (Ang):', rcut_qmmm

        dist     = 0.0_dp
        dist2    = rcut_qmmm * rcut_qmmm * Ang * Ang
        listqmmm = 1

        do qat = 1, na_qm
          iat = 1

          do ires = 1, nroaa
            dist = dist2_v2( rclas(1,qat) - cm(1,ires), &
                             rclas(2,qat) - cm(2,ires), &
                             rclas(3,qat) - cm(3,ires) )
            do jat = 1, atxres(ires)
              if ( dist2 > dist ) listqmmm(iat) = 0
              iat = iat +1
            enddo
          enddo
        enddo

        count = 0
        do iat = 1, na_mm
          if ( listqmmm(iat) == 0) count = count +1
        enddo

        write( 6, '(/a,2x,i5)' ) 'qm-mm: Number of QM-MM interacting atoms:', &
                                 count

        ! Open file and write listqmmm.
        call io_assign( iu )
        open( iu, file = fname, form = 'formatted', status = 'unknown' )

        do iat = 1, na_mm
          write(iu,*) listqmmm(iat)
        enddo

        call io_close( iu )

      else ! liqmmm = .false.
        write(6,'(/a)') 'qm-mm: Warning QM-MM cutoff too large.'
      endif
    endif

    ! block QM-MM
    found   = .false.
    bloqmmm = .false.

    ! Find file name and check if input exists.
    fname = trim(slabel) // '.blk'
    inquire( file = fname, exist = found )
    if ( found ) then
      call io_assign( iu )
      open( iu, file = fname, status = 'old' )

      ! read blockqmmm
      do iat = 1, na_mm
        read( iu, *, iostat = ios ) blockqmmm(iat)
        call check_ios( ios, '.blk' )
      enddo

      call io_close( iu )
      write( 6, '(/a)' ) 'qm-mm: Reading blocked QM-MM atoms from file.'
    else
      if ( .not. (blockmm_radius > 99.0_dp) ) bloqmmm = .true.
      if( bloqmmm ) then
        ! Fixing MM atoms beyond block cut off
        write( 6, '(/a,f12.6)' ) 'qm-mm: cut off radius Block (Ang):', &
                                 blockmm_radius
        dist         = 0.0_dp
        dist2        = blockmm_radius * blockmm_radius * Ang * Ang
        blockqmmm(:) = 1

        do qat = 1, na_qm
          iat = 1

          do ires = 1, nroaa
            dist = dist2_v2( rclas(1,qat) - cm(1,ires), &
                             rclas(2,qat) - cm(2,ires), &
                             rclas(3,qat) - cm(3,ires) )

            do jat = 1, atxres(ires)
              if ( dist2 > dist ) blockqmmm(iat) = 0
              iat = iat +1
            enddo
          enddo
        enddo

        count = 0
        do iat = 1, na_mm
          if ( .not. (abs(blockmm_radius) > 0.0_dp) ) blockqmmm(iat) = 1
          if ( blockqmmm(iat) == 0 ) count = count +1
        enddo

        write( 6, '(/a,2x,i5)' ) 'qm-mm: Number of QM-MM free atoms:', count

        ! Write blockQMMM to file.
        call io_assign( iu )
        open( iu, file = fname, form = 'formatted', status = 'unknown' )

        do iat = 1, na_mm
          write( iu, * ) blockqmmm(iat)
        enddo

        call io_close( iu )

      else ! bloqmmm=.false.
        write( 6, '(/a)' ) 'qm-mm: Warning Block cutoff too large.'
      endif
    endif
    call de_alloc( cm, 'cm', 'qmmm_lst_blk' )

    call pxfflush( 6 )
  end subroutine qmmm_lst_blk

  subroutine check_ios( ios, f_ext )
    !! Checks read status when attempting to read files.
    use sys, only : die
    implicit none
    integer         , intent(in) :: ios
      !! Read status.
    character(len=4), intent(in) :: f_ext
      !! File extention involved in read.

    if ( ios /= 0 ) call die( 'qm-mm: Problem reading from '//f_ext//' file.' )
  end subroutine check_ios

end module qmmm_list_block
